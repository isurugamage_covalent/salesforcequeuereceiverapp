﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IFSConnector.IFSConnector;
using IFSConnector.Model;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Newtonsoft.Json;

namespace CoreReceiverApp
{
   
   
    class Program
    {
        // Connection String for the namespace can be obtained from the Azure portal under the 
        // 'Shared Access policies' section.
        const string ServiceBusConnectionString = "Endpoint=sb://salesforcebusns.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=ecIJeKg8Z2u+96assbKYGT4GNxO/NcE4r6ePz+Z8dtg=";
        const string TopicName = "updateifs_topic";
        const string SubscriptionName = "updateifs_subscription";
        static ISubscriptionClient subscriptionClient;

        public static async Task Main(string[] args)
        {
            subscriptionClient = new SubscriptionClient(ServiceBusConnectionString, TopicName, SubscriptionName);

            Console.WriteLine("======================================================");
            Console.WriteLine("Press ENTER key to exit after receiving all the messages.");
            Console.WriteLine("======================================================");

            // Register subscription message handler and receive messages in a loop
            RegisterOnMessageHandlerAndReceiveMessages();

            Console.ReadKey();

            await subscriptionClient.CloseAsync();
        }

        static void RegisterOnMessageHandlerAndReceiveMessages()
        {
            // Configure the message handler options in terms of exception handling, number of concurrent messages to deliver, etc.
            var sessionHandlerOptions = new SessionHandlerOptions(ExceptionReceivedHandler)
            {
                // Maximum number of concurrent calls to the callback ProcessMessagesAsync(), set to 1 for simplicity.
                // Set it according to how many messages the application wants to process in parallel.
                MaxConcurrentSessions = 1,

                // Indicates whether the message pump should automatically complete the messages after returning from user callback.
                // False below indicates the complete operation is handled by the user callback as in ProcessMessagesAsync().
                AutoComplete = true
            };

            // Register the function that processes messages.
            subscriptionClient.RegisterSessionHandler(ProcessMessagesInSessionAsync, sessionHandlerOptions);
        }

        static async Task ProcessMessagesInSessionAsync(IMessageSession messageSession, Message message, CancellationToken token)
        {
            // Process the message.
            //Console.WriteLine($"Received message: SequenceNumber:{message.SystemProperties.SequenceNumber} Body:{Encoding.UTF8.GetString(message.Body)}");

            var obj = GetObject<Identifier>(message.Body);

            if (obj._Key != null)
            {
                switch (obj._Key)
                {
                    case "account":
                        //var account = GetObject<AccountMessage>(message.UserProperties);
                        var account = GetObject<AccountMessage>(message.Body);
                        ConnectIFS connectIFS = new ConnectIFS();
                        connectIFS.UpdateAccount(account);
                        break;
                    case "contact":
                        //Process
                        break;
                    default:
                        break;
                }
                await Task.CompletedTask;
            }

            // Complete the message so that it is not received again.
            // This can be done only if the subscriptionClient is created in ReceiveMode.PeekLock mode (which is the default).
            //await subscriptionClient.CompleteAsync(messageSession.SystemProperties.LockToken);
            //await Task.CompletedTask;

            // Note: Use the cancellationToken passed as necessary to determine if the subscriptionClient has already been closed.
            // If subscriptionClient has already been closed, you can choose to not call CompleteAsync() or AbandonAsync() etc.
            // to avoid unnecessary exceptions.
        }

        // Use this handler to examine the exceptions received on the message pump.
        static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        }

        //static T GetObject<T>(IDictionary<string, object> dict)
        //{
        //    Type type = typeof(T);
        //    var obj = Activator.CreateInstance(type);

        //    foreach (var kv in dict)
        //    {
        //        type.GetProperty(kv.Key).SetValue(obj, kv.Value);
        //    }
        //    return (T)obj;
        //}

        static T GetObject<T>(byte[] byteArrayContent)
        {
            string jsonBack = Encoding.UTF8.GetString(byteArrayContent);
            var objBack = JsonConvert.DeserializeObject<T>(jsonBack);
            return objBack;
        }
    }
}
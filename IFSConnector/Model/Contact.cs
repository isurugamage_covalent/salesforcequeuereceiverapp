﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IFSConnector.Model
{
    public class Contact
    {

        public string sf_id_ { get; set; }    
        public string cust_name_ { get; set; }
        public string language { get; set; }
        public string country { get; set; }
        public string type { get; set; }       
        public string phone { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IFSConnector.Model
{
    public class AccountMessage : Identifier
    {
        public string AccountID { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public string BillingCity { get; set; }
        public string BillingCountry { get; set; }
        public string BillingPostcode { get; set; }
        public string BillingState { get; set; }
        public string BillingStreet { get; set; }
        public string Country { get; set; }
        public string IFSCustomerID { get; set; }
        public string Language { get; set; }
        public string Phone { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingStreet { get; set; }
    }
}

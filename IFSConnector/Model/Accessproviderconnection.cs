﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFSConnector.Model
{
  public  class Accessproviderconnection
    {
        public string username { get; set; }
        public string password { get; set; }
        public string callbackurl { get; set; }
        public string apiswagger { get; set; }
        public string  version { get; set; }
    }
}

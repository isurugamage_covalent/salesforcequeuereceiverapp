﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace IFSConnector.Model
{
    public class Response
    {
        public HttpStatusCode StatusCode { get; set; }

        public dynamic ResponseBody { get; set; }
    }
}

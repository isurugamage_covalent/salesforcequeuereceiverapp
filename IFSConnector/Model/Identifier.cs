﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IFSConnector.Model
{
    public class Identifier
    {
        public string _Key { get; set; }
        public string SyncID { get; set; }
    }
}

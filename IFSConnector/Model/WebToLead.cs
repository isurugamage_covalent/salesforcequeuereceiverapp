﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFSConnector.Model
{
    public  class WebToLead
    {
        
        public string lead_id_ { get; set; }
        public string name_  { get; set; }
        public string contact_name_ { get; set; }
        public string language_ { get; set; }
        public string currency_  { get; set; }
        public string country_code_ { get; set; }
        public string zip_code_ { get; set; }
        public string email_ { get; set; }
        public string phone_ { get; set; }
        public string notes_ { get; set; }
        public string source_ { get; set; }
        public string source_detail_ { get; set; }
        public string web_enquiry_type_ { get; set; }
        public string utm_source_ { get; set; }
        public string utm_medium_ { get; set; }
        public string utm_campaign_ { get; set; }
        public string utm_content_ { get; set; }
        public string utm_ref_ { get; set; }
        public string referring_url_ { get; set; }
        public string gclid_ { get; set; }
        public string web_lead_flag_ { get; set; }
        public string direct_response_flag_ { get; set; }
        public string opt_in_out_ { get; set; }
        public string promotion_ { get; set; }
    }
}

﻿using Ifs.Fnd;
using Ifs.Fnd.AccessProvider;
using Ifs.Fnd.AccessProvider.PLSQL;
using Ifs.Fnd.Data;
using IFSConnector.Interface;
using IFSConnector.Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IFSConnector.IFSConnector
{
    public class ConnectIFS
    {

        public FndConnection ifsonnection()
        {

            try
            {
                Accessproviderconnection connectorval = new Accessproviderconnection
                {
                    username = "covalent_int",
                    password = "COVALENT",
                    callbackurl = "https://ifst.chemigraphic.co.uk:50080",
                };
               
                FndConnection conn = new FndConnection(connectorval.callbackurl, connectorval.username, connectorval.password);
                conn.CatchExceptions = false;
                return conn;
            }
            catch (Exception e)
            {
                return null;
            }

        }
       
    
       
        public async Task<object> PostContact(Contact contact)
        {
            
            
                var con = ifsonnection();
            FndPLSQLCommand cmd = new FndPLSQLCommand(con, "ifsapp.Covalent_Salesforce_Api.Sync_Customer_Info(:sf_id_,:cust_name_,:language_,:country_,:type_,:phone_)");
          
             cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "sf_id_", new FndTextAttribute(contact.sf_id_)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "cust_name_", new FndTextAttribute(contact.cust_name_)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "language_", new FndTextAttribute(contact.language)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "country_", new FndTextAttribute(contact.country)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "type_", new FndTextAttribute(contact.type)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "phone_", new FndTextAttribute(contact.phone)));
            try
            {
                cmd.ExecuteNonQuery();
                // cmd.

               
            }
            catch (FndException e)
            {
              
            }
            return "sucess";
        }

        public async Task<object> UpdateAccount(AccountMessage account)
        {
            var con = ifsonnection();
            FndPLSQLCommand cmd = new FndPLSQLCommand(con, "ifsapp.Covalent_Salesforce_Api.Sync_Customer_Info(:sync_id_,:sf_id_,:cust_name_,:language_,:country_,:type_,:phone_)");

            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "sync_id_", new FndTextAttribute(account.SyncID)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "sf_id_", new FndTextAttribute(account.AccountID)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "cust_name_", new FndTextAttribute(account.AccountName)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "language_", new FndTextAttribute(account.Language)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "country_", new FndTextAttribute(account.Country)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "type_", new FndTextAttribute(account.AccountType)));
            cmd.BindVariables.Add(new FndBindVariable(FndBindVariableDirection.In, "phone_", new FndTextAttribute(account.Phone)));
            try
            {
                cmd.ExecuteNonQuery();
                // cmd.
                return "success";

            }
            catch (FndException ex)
            {
                return ex;
            }
        }

    }
}
